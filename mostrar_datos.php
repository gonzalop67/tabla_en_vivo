<?php
include_once "./conexion.php";

$consulta = mysqli_query($conexion, "SELECT * FROM users");

echo "
<table border='1px' align='center'>
    <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Eliminar</th>
    </tr>
";

while ($registro = mysqli_fetch_object($consulta)) {
    echo "
    <tr>
        <td>" . $registro->id_users . "</td>
        <td id='nombre_usuario' data-id_usuario='" . $registro->id_users . "' contenteditable>" . $registro->nombre_users . "</td>
        <td id='apellido_usuario' data-id_apellido='" . $registro->id_users . "' contenteditable>" . $registro->apellido_users . "</td>
        <td><button id='eliminar' data-id='" . $registro->id_users . "'>Eliminar</button></td>
    </tr>";
}

echo "
    <tr>
        <td></td>
        <td id='nombre_add' contenteditable></td>
        <td id='apellido_add' contenteditable></td>
        <td><button id='add'>Agregar</button></td>
    </tr>
";

echo "</table>";
