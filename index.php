<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabla en vivo con PHP y AJAX</title>
    <link rel="stylesheet" href="./estilos.css">
    <script src="./jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            // OBTENIENDO LOS DATOS
            function obtenerDatos() {
                $.ajax({
                    type: "GET",
                    url: "mostrar_datos.php",
                    success: function(response) {
                        $("#result").html(response);
                    }
                });
            }

            obtenerDatos();
            // OBTENIENDO LOS DATOS

            // AGREGANDO DATOS
            $(document).on("click", "#add", function() {
                var nombre_add = $("#nombre_add").text();
                var apellido_add = $("#apellido_add").text();

                $.ajax({
                    type: "POST",
                    url: "insertar.php",
                    data: {
                        nombre: nombre_add,
                        apellido: apellido_add
                    },
                    success: function(response) {
                        obtenerDatos();
                        alert(response);
                    }
                });
            });

            // ACTUALIZANDO DATOS

            function actualizar_datos(id, texto, columna) {
                $.ajax({
                    type: "POST",
                    url: "actualizar.php",
                    data: {
                        id: id,
                        texto: texto,
                        columna: columna
                    },
                    success: function(response) {
                        obtenerDatos();
                    }
                });
            }

            $(document).on("blur", "#nombre_usuario", function() {
                var id = $(this).data("id_usuario");
                var nombre = $(this).text();

                actualizar_datos(id, nombre, "nombre_users");
            });

            $(document).on("blur", "#apellido_usuario", function() {
                var id = $(this).data("id_apellido");
                var apellido = $(this).text();

                actualizar_datos(id, apellido, "apellido_users");
            });

            $(document).on("click", "#eliminar", function() {
                if (confirm("Está seguro de que desea eliminar este registro.")) {
                    var id = $(this).data("id");
                    $.ajax({
                        type: "POST",
                        url: "eliminar.php",
                        data: {
                            id: id
                        },
                        success: function(response) {
                            obtenerDatos();
                        }
                    });
                }
            });
        });
    </script>
</head>

<body>
    <div id="container">
        <div id="result">

        </div>
    </div>
</body>

</html>